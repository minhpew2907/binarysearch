﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GiaiThut
{
    public class BinarySearch
    {
        public bool BinarySearching(int[] arr, int target)
        {
            int left = 0;
            int right = arr.Length - 1;
            int mid = 0;

            do
            {
                mid = left + right / 2;
                if (arr[mid] == target)
                {
                    return true;
                }
                if (arr[mid] > target)
                {
                    right = mid - 1;
                }
                else
                {
                    left = mid + 1;
                }
            } while (left <= right);
            return false;
        }
    }
}
